package twpathashala51;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class testAccount {
    @Test
    public void putTenRupeesInAccount() {
        Account myAccount = new Account();
        assertEquals(Money.money(10), myAccount.putMoney(Money.money(10)));
        assertEquals(Money.money(10), myAccount.getBalance());
        assertEquals(Money.money(0), myAccount.getCredit());
    }

    @Test
    public void putTwentyRupeesInAccountAndGetTotalAmountInAccount() {
        Account myAccount = new Account(Money.money(20));
        assertEquals(Money.money(30), myAccount.putMoney(Money.money(10)));
        assertEquals(Money.money(30), myAccount.getBalance());
        assertEquals(Money.money(0), myAccount.getCredit());
    }

    @Test
    public void putThirtyRupeesInAccountAndGetTenRupees() throws InsufficientBalanceException {
        Account myAccount = new Account(Money.money(30));
        assertEquals(Money.money(10), myAccount.getMoney(Money.money(10)));
        assertEquals(Money.money(20), myAccount.getBalance());
        assertEquals(Money.money(0), myAccount.getCredit());
    }

    @Test
    public void getZeroMoneyFromAccount() {
        Account myAccount = new Account(Money.money(20));
        assertEquals(Money.money(0), myAccount.getMoney(Money.money(0)));
        assertEquals(Money.money(20), myAccount.getBalance());
        assertEquals(Money.money(0), myAccount.getCredit());
    }

    @Test
    public void getMoneyFromEmptyAccount() {
        DummyAccountListeners customer = new DummyAccountListeners();
        List<NotifyInterestedUser> listOfListeners = new ArrayList<>();
        listOfListeners.add(customer);
        Account myAccount = new Account(listOfListeners);
        assertEquals(Money.money(10), myAccount.getMoney(Money.money(10)));
        assertEquals("Over Drawn From Account", customer.getAccountstatus());
        assertEquals(Money.money(0), myAccount.getBalance());
        assertEquals(Money.money(10), myAccount.getCredit());
    }

    @Test
    public void getMoneyFromDeficientAccount() {
        DummyAccountListeners customer = new DummyAccountListeners();
        DummyAccountListeners listenerOne = new DummyAccountListeners();
        List<NotifyInterestedUser> listOfListeners = new ArrayList<>();
        listOfListeners.add(listenerOne);
        listOfListeners.add(customer);
        Account myAccount = new Account(Money.money(10), listOfListeners);
        assertEquals(Money.money(100), myAccount.getMoney(Money.money(100)));
        assertEquals("Over Drawn From Account", customer.getAccountstatus());
        assertEquals(Money.money(0), myAccount.getBalance());
        assertEquals(Money.money(90), myAccount.getCredit());

    }

    @Test
    public void putMoneyWhenThereIsCredit() {
        DummyAccountListeners customer = new DummyAccountListeners();
        DummyAccountListeners listenerOne = new DummyAccountListeners();
        List<NotifyInterestedUser> listOfListeners = new ArrayList<>();
        listOfListeners.add(listenerOne);
        listOfListeners.add(customer);
        Account myAccount = new Account(Money.money(0), Money.money(20), listOfListeners);
        assertEquals(Money.money(80), myAccount.putMoney(Money.money(100)));
        assertEquals(Money.money(80), myAccount.getBalance());
        assertEquals(Money.money(0), myAccount.getCredit());

    }

    @Test
    public void getMoreMoneyWhenThereIsCredit() {
        DummyAccountListeners customer = new DummyAccountListeners();
        DummyAccountListeners listenerOne = new DummyAccountListeners();
        List<NotifyInterestedUser> listOfListeners = new ArrayList<>();
        listOfListeners.add(listenerOne);
        listOfListeners.add(customer);
        Account myAccount = new Account(Money.money(0), Money.money(20), listOfListeners);
        assertEquals(Money.money(100), myAccount.getMoney(Money.money(100)));
        assertEquals("Over Drawn From Account", customer.getAccountstatus());
        assertEquals("Over Drawn From Account", listenerOne.getAccountstatus());
        assertEquals(Money.money(0), myAccount.getBalance());
        assertEquals(Money.money(120), myAccount.getCredit());

    }

    @Test
    public void getNotifiedWhenUserIsUsingCredit() {
        DummyAccountListeners customer = new DummyAccountListeners();
        List<NotifyInterestedUser> listOfListeners = new ArrayList<>();
        listOfListeners.add(customer);
        Account myAccount = new Account(listOfListeners);
        assertEquals(Money.money(10), myAccount.getMoney(Money.money(10)));
        assertEquals("Over Drawn From Account", customer.getAccountstatus());
        Money balanceMoney = myAccount.getBalance();
        assertEquals(Money.money(0), balanceMoney);
        assertEquals(Money.money(10), myAccount.getCredit());
    }

    @Test
    public void getNotifiedWhenUserWithCreditIsUsingMoreCredit() {
        DummyAccountListeners customer = new DummyAccountListeners();
        DummyAccountListeners listenerOne = new DummyAccountListeners();
        DummyAccountListeners listenerTwo = new DummyAccountListeners();
        List<NotifyInterestedUser> listOfListeners = new ArrayList<>();
        listOfListeners.add(listenerOne);
        listOfListeners.add(customer);
        listOfListeners.add(listenerTwo);
        Account myAccount = new Account(Money.money(10), listOfListeners);
        assertEquals(Money.money(100), myAccount.getMoney(Money.money(100)));
        assertEquals("Over Drawn From Account", customer.getAccountstatus());
        assertEquals("Over Drawn From Account", listenerOne.getAccountstatus());
        assertEquals("Over Drawn From Account", listenerTwo.getAccountstatus());
        Money balanceMoney = myAccount.getBalance();
        assertEquals(Money.money(0), balanceMoney);
        assertEquals(Money.money(90), myAccount.getCredit());
    }

    @Test
    public void notifyMultipleUsersWhenOverdrawn() {
        DummyAccountListeners customer = new DummyAccountListeners();
        DummyAccountListeners listenerOne = new DummyAccountListeners();
        List<NotifyInterestedUser> listOfListeners = new ArrayList<>();
        listOfListeners.add(listenerOne);
        listOfListeners.add(customer);
        Account myAccount = new Account(Money.money(100), listOfListeners);
        assertEquals(Money.money(50), myAccount.getMoney(Money.money(50)));
        assertEquals(Money.money(50), myAccount.getBalance());
        assertEquals(Money.money(0), myAccount.getCredit());
    }

    public static class DummyAccountListeners implements NotifyInterestedUser {
        private String accountStatus;

        public String getAccountstatus() {
            return accountStatus;
        }

        @Override
        public void sendNotification(String notification) {
            accountStatus = notification;
        }
    }
}
