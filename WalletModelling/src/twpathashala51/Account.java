package twpathashala51;

import java.util.ArrayList;
import java.util.List;

//To hold money details of the account owner
public class Account {
    private Money myMoney;
    private Money myCredit;
    private List<NotifyInterestedUser> interestedListeners = new ArrayList<>();

    public Account() {
        this.myMoney = Money.money(0);
        this.myCredit = Money.money(0);
    }

    public Account(Money myMoney) {
        this.myMoney = myMoney;
        this.myCredit = Money.money(0);
    }

    public Account(Money myMoney, List<NotifyInterestedUser> accountListeners) {
        this.myMoney = myMoney;
        this.myCredit = Money.money(0);
        this.interestedListeners = accountListeners;
    }

    public Account(List<NotifyInterestedUser> accountListeners) {
        this.myMoney = Money.money(0);
        this.myCredit = Money.money(0);
        this.interestedListeners = accountListeners;

    }

    public Account(Money myMoney, Money myCredit, List<NotifyInterestedUser> accountListeners) {
        this.myMoney = myMoney;
        this.myCredit = myCredit;
        this.interestedListeners = accountListeners;
    }

    public Money putMoney(Money money) {
        if (money.isGreaterThan(myCredit)) {
            myMoney = this.myMoney.addMoney(money);
            myMoney = this.myMoney.subtractMoney(myCredit);
            myCredit = Money.money(0);
        } else {
            myCredit = myCredit.subtractMoney(money);
        }
        return myMoney;
    }

    public Money getMoney(Money money) {
        Money requestedMoney = money;
        if (this.myMoney.isGreaterThan(money)) {
            myMoney = this.myMoney.subtractMoney(money);
        } else {
            money = money.subtractMoney(this.myMoney);
            myMoney = Money.money(0);
            myCredit = money.addMoney(myCredit);

            for (NotifyInterestedUser eachListener : interestedListeners) {
                eachListener.sendNotification("Over Drawn From Account");
            }
        }
        return requestedMoney;
    }

    @Override
    public String toString() {
        return "Account{" +
                "myMoney=" + myMoney +
                '}';
    }

    public Money getBalance() {
        return myMoney;
    }

    public Money getCredit() {
        return myCredit;
    }
}
