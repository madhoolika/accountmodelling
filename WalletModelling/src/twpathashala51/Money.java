package twpathashala51;

//Represents the resource used to do transactions
public class Money {
    private final double value;

    private Money(double value) {
        this.value = value;
    }

    public static Money money(double value) {
        return new Money(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return value == money.value;
    }

    public boolean isGreaterThan(Money other) {
        return this.value > other.value;
    }

    @Override
    public int hashCode() {
        return (int) value;
    }

    @Override
    public String toString() {
        return "Money{" +
                "value=" + value +
                '}';
    }

    public Money addMoney(Money other) {
        return Money.money(this.value + other.value);
    }

    public Money subtractMoney(Money other) {
        return Money.money(this.value - other.value);
    }
}
